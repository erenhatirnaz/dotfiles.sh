![./dotfiles.sh](dotfiles.gif)

# ./dotfiles.sh
> Organize your dotfiles repository

A git-like bash script for organize your dotfiles repository.

**Note: Adding directories is not supported yet.**

## Usage

```bash
$ cd path/to/dotfiles_repository
$ wget https://gitlab.com/erenhatirnaz/dotfiles.sh/raw/master/dotfiles.sh
$ chmod u+x ./dotfiles.sh
$ ./dotfiles.sh init
$ ./dotfiles.sh help
```

## Demonstration
[![asciicast](https://asciinema.org/a/3AudK5FXzIh8wfOy7m05wcF4l.svg)](https://asciinema.org/a/3AudK5FXzIh8wfOy7m05wcF4l)

## Features

* No need to configurate!
* `add` a dotfile to the repository
* `remove` a dotfile from the repository
* `list` all dotfiles in the repository
* `diff` list which dotfiles are differ
* `backup` a dotfile to the repository
* `restore` a dotfile from the repository

## TODOs

* [ ] Adding directory support

## License

GNU General Public License v3
