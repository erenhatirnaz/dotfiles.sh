#!/bin/bash
# shellcheck disable=SC2120,SC2230,SC2119
set +H

# SCRIPT INFO
readonly script_name="$0"
readonly version=0.1.0
readonly db_filename=".dotfiles.db"

# TOOLS
readonly DIFF="$(which diff) --color=auto"
readonly GREP="$(which grep) --color=auto"

# COLORS
readonly RESET=$'\033[0m'
readonly RED=$'\033[1;31m'
readonly BLUE=$'\033[0;34m'
readonly GREEN=$'\033[1;32m'

# OUTPUT TAGS
readonly ASK=$BLUE"[ASK]:"$RESET
readonly INFO=$BLUE"[INFO]:"$RESET
readonly ERROR=$RED"[ERROR]:"$RESET
readonly SUCCESS=$GREEN"[SUCCESS]:"$RESET

# Private Functions
err() {
	echo -e "$ERROR $*" >&2
	exit 1
}

check_initialized() {
	if [[ ! -e $db_filename ]]; then
		err "$script_name isn't initialized! Run '$script_name init' command."
	fi
}

check_empty_repo() {
	[[ ! -s $db_filename ]] && err "Repository is empty!"
}

check_local_dotfile_exist_on_repository() {
	local file="$1"
	local flags="$2"

	[[ ! -e "$file" ]] && err "$file is not found!"

	local -r global_dotfile=$(get_global_dotfile "$file")
	if [[ -n $global_dotfile && "$flags" = "--exit-if-exists" ]]; then
		err "$global_dotfile is already exists in repository!"
	elif [[ -z $global_dotfile && "$flags" = "--exit-if-not-exists" ]]; then
		err "$global_dotfile is not exists in repository!"
	fi
}

get_global_dotfile() {
	local local_dotfile="$1"

	$GREP -wi "$local_dotfile" "$db_filename"
}

# Public Functions
# ./dotfiles.sh help <command>
help() {
	if [[ -z $1 ]]; then
		echo -e "Usage:"\\n\
		"    $script_name [-v | --version] [-h | --help] <command> [<args>]"\\n\
		\\n\
		"The script for dotfiles repository management. Run '$script_name help"\\n\
		"terminology' for learn terminology details."\\n\
		\\n\
		"Commands:"\\n\
		"      init    Create an empty $script_name repository"\\n\
		"       add    Add given global dotfile to repository"\\n\
		"    remove    Remove given local dotfile in repository"\\n\
		"      list    List all local dotfiles and exit"\\n\
		"      diff    Compore local dotfiles with global dotfiles"\\n\
		"    backup    Copy differ global dotfile(s) to repository"\\n\
		"   restore    Copy local dotfile(s) to differ global dotfile paths"\\n\
		\\n\
		"Options:"\\n\
		"    -v, --version    Show program's version number and exit"\\n\
		"    -h,    --help    Show this help message and exit"\\n\
		\\n\
		"Try '$script_name help <command>' for details."\\n\
		\\n\
		"License:"\\n\
		"    GNU General Public License v3"\\n\
		\\n\
		"Author:"\\n\
		"    Eren Hatırnaz <erenhatirnaz@hotmail.com.tr>"
	else
		case $1 in
		init)
		echo -e "Usage:"\\n\
		"    $script_name init [--force]"\\n\
		\\n\
		"Description:"\\n\
		"    This command creates an empty $script_name repository - basically a"\\n\
		"    $db_filename for store global dotfiles paths line by line. If you want to"\\n\
		"    change database filename, edit \$db_filename variable in $script_name with"\\n\
		"    you favourite editor."\\n\
		\\n\
		"Options:"\\n\
		"    --force    Remove the exist $db_filename and creates empty one."
		;;
		add)
		echo -e "Usage:"\\n\
		"    $script_name add <path/to/global_dotfile>"\\n\
		\\n\
		"Description:"\\n\
		"    This command copies given global dotfile to where $script_name initialized"\\n\
		"    and appends the full path of global dotfile to $db_filename."
		;;
		remove)
		echo -e "Usage:"\\n\
		"    $script_name remove <local_dotfile>"\\n\
		\\n\
		"Description:"\\n\
		"    This command deletes given local dotfile on where $script_name initialized"\\n\
		"    and removes the full path of global dotfile in $db_filename."
		;;
		list)
		echo -e "Usage:"\\n\
		"    $script_name list"\\n\
		\\n\
		"Description:"\\n\
		"    This command prints the content of $db_filename."
		;;
		diff)
		echo -e "Usage:"\\n\
		"    $script_name diff [<local_dotfile>]"\\n\
		\\n\
		"Description:"\\n\
		"    This command compares local dotfiles with global dotfiles and prints differ"\\n\
		"    ones."\\n\
		\\n\
		"Options:"\\n\
		"    <local_dotfile>    If local dotfile is given, just compares that one with"\\n\
		"                       detailed output."
		;;
		backup)
		echo -e "Usage:"\\n\
		"    $script_name backup [<path/to/global_dotfile>]"\\n\
		\\n\
		"Description:"\\n\
		"    This command copies all or just given changed global dotfiles to repository."\\n\
		\\n\
		"Options:"\\n\
		"    <path/to/global_dotfile>    If global dotfile is given, just copies given."\\n\
		"                                If given global dotfile isn't changed then do"\\n\
		"                                nothing."
		;;
		restore)
		echo -e "Usage:"\\n\
		"    $script_name restore [<local_dotfile>]"\\n\
		\\n\
		"Description:"\\n\
		"    This command copies all or just given local dotfiles to changed global"\\n\
		"    dotfile."\\n\
		\\n\
		"Options:"\\n\
		"    <local_dotfile>    If local dotfile is given, just copies given. If given"\\n\
		"                       global dotfile isn't changed then do nothing."
		;;
		terminology)
		echo -e "Terminology:"\\n\
		"    * Global dotfile: Means real dotfile. For example: ~/.vimrc,"\\n\
		"      ~/.config/rofi/config. These files copies to repository by $script_name."\\n\
		\\n\
		"    * Local dotfile: Means copy of global dotfile. These files stores on where"\\n\
		"      $script_name initialized."\\n\
		;;
		esac
	fi
	exit
}

# ./dotfiles.sh [-v | --version]
version() {
	echo "$script_name version $version"
}

# ./dotfiles.sh init
init() {
	local flags="$1"
	if [[ ! -e $db_filename || $flags = "--force" ]]; then # ./dotfiles.sh init [--force]
		rm "$db_filename" 2> /dev/null
		touch "$db_filename"
		echo -e "$SUCCESS Initialized empty $script_name repository!"
	else # ./dotfiles.sh init
		echo -e "$INFO $script_name is already initialized! If you want re-initialize"
		echo "repository, run '$script_name init $RED--force$RESET' command."
	fi
}

# ./dotfiles.sh add <path/to/global_dotfile>
add() {
	local global_dotfile="$1"

	check_local_dotfile_exist_on_repository "$global_dotfile" --exit-if-exists

	cp "$global_dotfile" .
	echo "$global_dotfile" >> "$db_filename"
	echo -e "$SUCCESS $global_dotfile is added to repository!"
}

# ./dotfiles.sh remove <local_dotfile>
remove() {
	local -r local_dotfile=$(basename "$1")

	check_local_dotfile_exist_on_repository "$local_dotfile" --exit-if-not-exists

	printf "%s " "$ASK"
	read -rp "Remove dotfile '$local_dotfile'? (y/N): " -n1 choice
	choice=${choice:-n}
	[[ ! $choice =~ [Yy]$ ]] && echo -e \\n$RED"Operation aborted"$RESET && exit

	sed -i /"$local_dotfile"/d "$db_filename"
	rm "$local_dotfile"
	echo -e "\\n$SUCCESS $local_dotfile is removed in repository!"
}

# ./dotfiles.sh list
list() {
	if [[ -s "$db_filename" ]]; then
		cat -n "$db_filename"
	else
		echo -e "$INFO Repository is empty!"
	fi
}

diff() {
	check_empty_repo
	if [[ -n $1 ]]; then # ./dotfiles.sh diff <local_dotfile>
		local local_dotfile="$1"
		check_local_dotfile_exist_on_repository "$local_dotfile" --exit-if-not-exists

		local -r global_dotfile=$(get_global_dotfile "$local_dotfile")
		$DIFF -Zus "$local_dotfile" "$global_dotfile"
	else # ./dotfiles.sh diff
		local -r dotfiles=$(list | cut -f2)
		total_differs=0
		for global_dotfile in $dotfiles
		do
			local_dotfile=$(basename "$global_dotfile")
			if [[ ! -e $global_dotfile ]]; then
				echo "$global_dotfile"
			elif [[ $($DIFF -qZ "$global_dotfile" "$local_dotfile") ]]; then
				echo "$global_dotfile"
				((total_differs++))
			fi
		done
		if [[ $total_differs -gt 0 ]]; then
			echo -e "$INFO Total differ dotfile: $total_differs"
		else
			echo -e "$SUCCESS Everything is up-to-date!"
	  exit
		fi
	fi
}

backup() {
	check_empty_repo
	local global_dotfile="$1"
	if [[ -n $global_dotfile ]]; then # ./dotfiles.sh backup <global_dotfile>
		local_dotfile=$(basename "$global_dotfile")
		if [[ ! -e $local_dotfile ]]; then
			err "$global_dotfile isn't added before to this repository."\
				"Try '$script_name add $global_dotfile' command." | fold -s
		fi
		cp "$global_dotfile" .
		echo -e "$SUCCESS $global_dotfile copied to $local_dotfile."
	else # ./dotfiles.sh backup
	diff

	printf "%s " "$ASK"
	read -rp "Backup all differ(s) from global dotfiles to repository? (y/N): " -n1 choice
	choice=${choice:-n}
	echo -e \\n
	[[ ! $choice =~ [Yy]$ ]] && echo -e $RED"Operation aborted"$RESET && exit

		local -r diff_files=$(diff | sed /INFO/d)
		for global_dotfile in $diff_files
		do
			local_dotfile=$(basename "$global_dotfile")
			cp "$global_dotfile" .
		echo -e "$SUCCESS $global_dotfile copied to $local_dotfile." | fold -s
		done
	fi
}

restore() {
	check_empty_repo
	local local_dotfile="$1"
	if [[ -n $local_dotfile ]]; then # ./dotfiles.sh restore <local_dotfile>
		check_local_dotfile_exist_on_repository "$local_dotfile" --exit-if-not-exists
		global_dotfile=$(get_global_dotfile "$local_dotfile")
		cp "$local_dotfile" "$global_dotfile"
		echo -e "$SUCCESS $local_dotfile copied to $global_dotfile." | fold -s
	else # ./dotfiles.sh restore
	diff

	printf "%s " "$ASK"
	read -rp "Restore all differ(s) from repository to global dotfiles? (y/N): " -n1 choice
	echo -e \\n
	[[ ! $choice =~ [Yy]$ ]] && echo -e $RED"Operation aborted"$RESET && exit

	local -r diff_files=$(diff | sed /INFO/d)
	for global_dotfile in $diff_files
	do
		local_dotfile=$(basename "$global_dotfile")
		cp "$local_dotfile" "$global_dotfile"
		echo -e "$SUCCESS $local_dotfile copied to $global_dotfile." | fold -s
	done
	fi
}

# Allowed commands or arguments without initialize
readonly allowed_commands="init help -v --version"

# Disallowed commands without argument
readonly disallowed_commands="add remove"

cmd=${1:-"help"}
[[ ! $allowed_commands = *$cmd* ]] && check_initialized
[[ $cmd = "-v" || $cmd = "--version" ]] && version && exit
[[ $cmd = "-h" || $cmd = "--help" ]] && help && exit

args=${2:-}
[[ $disallowed_commands = *$cmd* && -z $args ]] && help "$cmd"

$cmd "$args" && exit
